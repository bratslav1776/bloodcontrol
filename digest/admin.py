from django.contrib import admin
from .models import Town, Raion, Spec, Hospital

admin.site.register(Town)
admin.site.register(Raion)
admin.site.register(Spec)
admin.site.register(Hospital)

