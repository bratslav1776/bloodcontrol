from django.shortcuts import render
from django.http import HttpResponse
from .models import Town, Raion, Spec, Hospital

def home(request):


    data = {
        'hospitals': Hospital.objects.order_by('pk'),
        'title': "Лечебные организации"
    }

    return render(request, 'digest/index.html', data)

def about(request):
    return render(request, 'digest/about.html', {'title': 'Страница о нас'})
