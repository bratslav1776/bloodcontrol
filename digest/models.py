from django.db import models

class Town(models.Model):
    Name = models.CharField(max_length=20,verbose_name='Город')
    Text = models.TextField(blank=True, null=True,verbose_name='Описание')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True,verbose_name='Пользователь')  

    def __str__(self) -> str:
        return self.Name  

    class Meta: 
        ordering = ["Name"] 
        verbose_name_plural = 'Города'
        verbose_name = 'Город'
		
class Raion(models.Model):
    Name = models.CharField(max_length=20,verbose_name='Район')
    Text = models.TextField(blank=True, null=True,verbose_name='Описание')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True,verbose_name='Пользователь')  

    def __str__(self) -> str:
        return self.Name  

    class Meta: 
        ordering = ["Name"] 
        verbose_name_plural = 'Районы'
        verbose_name = 'Район'

class Spec(models.Model):        
    Name = models.CharField(max_length=20,verbose_name='Специализация медперсонала')
    Text = models.TextField(blank=True, null=True,verbose_name='Описание')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True,verbose_name='Пользователь')  

    def __str__(self) -> str:
        return self.Name  

    class Meta: 
        ordering = ["Name"] 
        verbose_name_plural = 'Специализации медперсонала'
        verbose_name = 'Специализация медперсонала'
		        
class Hospital(models.Model):        
    Name = models.CharField(max_length=200,verbose_name='Название организации')
    Text = models.TextField(blank=True, null=True,verbose_name='Описание')
    town = models.ForeignKey(Town, on_delete=models.SET_NULL, blank=True,null=True,verbose_name='Город')  
    raion = models.ForeignKey(Raion, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Район')  
    adress = models.TextField( blank=True,null=True,verbose_name='Адрес')  
    kol_in_page = models.IntegerField(verbose_name='Позиция в строке', default=0)

    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True,verbose_name='Пользователь')  

    def __str__(self) -> str:
        return self.Name  

    class Meta: 
        ordering = ["Name"] 
        verbose_name_plural = 'Мед учереждения'
        verbose_name = 'Медицинское учереждение' 